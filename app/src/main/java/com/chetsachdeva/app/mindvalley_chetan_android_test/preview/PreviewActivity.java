package com.chetsachdeva.app.mindvalley_chetan_android_test.preview;

import android.graphics.Color;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.chetsachdeva.app.mindvalley_chetan_android_test.R;
import com.chetsachdeva.app.mindvalley_chetan_android_test.base.BaseActivity;
import com.chetsachdeva.app.mindvalley_chetan_android_test.utils.Constants;
import com.facebook.drawee.view.SimpleDraweeView;

import butterknife.Bind;

public class PreviewActivity extends BaseActivity {

    @Bind(R.id.sdv_preview)
    SimpleDraweeView sdvPreview;

    String imageUrl;

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_preview;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getIntent().hasExtra(Constants.IMAGE_URL)) {
            imageUrl = getIntent().getStringExtra(Constants.IMAGE_URL);
        }

        sdvPreview.setImageURI(Uri.parse(imageUrl));
    }
}
