package com.chetsachdeva.app.mindvalley_chetan_android_test.adapters;

import android.content.Context;
import android.graphics.Color;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chetsachdeva.app.mindvalley_chetan_android_test.R;
import com.chetsachdeva.app.mindvalley_chetan_android_test.pins.PinsContract;
import com.chetsachdeva.app.mindvalley_chetan_android_test.utils.AppUtils;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.Pin;
import com.chetsachdeva.app.mindvalley_chetan_android_test.widgets.CustomTextView;
import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by chetsachdeva on 05/08/16.
 */
public class PinsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Pin> pinsList;
    PinsContract.View pinsView;
    Context mContext;

    private final int ITEM_PROGRESS = -1;


    public PinsAdapter(List<Pin> pinsList, PinsContract.View pinsView, Context context) {
        this.pinsList = pinsList;
        this.mContext = context;
        this.pinsView = pinsView;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_PROGRESS) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_progress, parent, false);
            return new ProgressViewHolder(v);
        } else {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pin, parent, false);
            return new PinsViewHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (null == pinsList.get(position)) {
            return ITEM_PROGRESS;
        } else {
            return super.getItemViewType(position);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PinsViewHolder) {
            PinsViewHolder pinsViewHolder = (PinsViewHolder) holder;
            Pin pin = pinsList.get(position);

            if (null != pin) {
                if (null != pin.getCategories() && pin.getCategories().size() > 0) {
                    pinsViewHolder.tvCategory.setVisibility(View.VISIBLE);
                    pinsViewHolder.tvCategory.setText(AppUtils.getCategoryAsString(pin.getCategories()));
                } else {
                    pinsViewHolder.tvCategory.setVisibility(View.GONE);
                }

                if (!TextUtils.isEmpty(pin.getColor())) {
                    pinsViewHolder.sdvPin.setBackgroundColor(Color.parseColor(pin.getColor()));
                } else {
                    pinsViewHolder.sdvPin.setBackgroundColor(ContextCompat.getColor(mContext, R.color.lightGrey));
                }
                if (pin.getHeight() != 0 && pin.getWidth() != 0) {
                    pinsViewHolder.sdvPin.getLayoutParams().height = AppUtils.pxToDp(mContext, pin.getHeight());
                } else {
                    pinsViewHolder.sdvPin.getLayoutParams().height = AppUtils.dpToPx(mContext, 240);
                }
                if (null != pin.getUrls() && !TextUtils.isEmpty(pin.getUrls().getRegular())) {
                    pinsViewHolder.sdvPin.setImageURI(Uri.parse(pin.getUrls().getRegular()));
                } else {
                    pinsViewHolder.sdvPin.setImageURI(null);
                }
                if (null != pin.getUser()) {
                    if (null != pin.getUser().getProfile_image() && !TextUtils.isEmpty(pin.getUser().getProfile_image().getSmall())) {
                        pinsViewHolder.sdvPp.setImageURI(Uri.parse(pin.getUser().getProfile_image().getSmall()));
                    } else {
                        pinsViewHolder.sdvPp.setImageURI(null);
                    }
                    if (!TextUtils.isEmpty(pin.getUser().getName())) {
                        pinsViewHolder.tvName.setText(pin.getUser().getName());
                    } else {
                        pinsViewHolder.tvName.setText("Name");
                    }
                    if (!TextUtils.isEmpty(pin.getUser().getUsername())) {
                        pinsViewHolder.tvDetails.setText(pin.getUser().getUsername());
                    } else {
                        pinsViewHolder.tvDetails.setText("Pin");
                    }
                }
            }
            setListeners(position, pinsViewHolder);
        }
    }

    public void setListeners(final int position, PinsViewHolder pinsViewHolder) {
        pinsViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pinsView.onPinClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return pinsList.size();
    }

    public class PinsViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.sdv_pin)
        SimpleDraweeView sdvPin;
        @Bind(R.id.sdv_pp)
        SimpleDraweeView sdvPp;
        @Bind(R.id.tv_category)
        CustomTextView tvCategory;
        @Bind(R.id.tv_name)
        CustomTextView tvName;
        @Bind(R.id.tv_details)
        CustomTextView tvDetails;

        public PinsViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public class ProgressViewHolder extends RecyclerView.ViewHolder {

        public ProgressViewHolder(View view) {
            super(view);
        }
    }
}
