package com.chetsachdeva.app.mindvalley_chetan_android_test.base;

/**
 * Created by chetsachdeva on 13/11/16.
 */

public interface BaseView {

    void showProgress();

    void hideProgress();

    void showToast(String message);

}
