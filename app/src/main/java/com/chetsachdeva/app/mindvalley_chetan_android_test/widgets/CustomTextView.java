package com.chetsachdeva.app.mindvalley_chetan_android_test.widgets;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.LruCache;
import android.widget.TextView;

import com.chetsachdeva.app.mindvalley_chetan_android_test.R;


public class CustomTextView extends TextView {

    private static LruCache<String, Typeface> mTypefaceCache = new LruCache<>(15);

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        //Typeface.createFromAsset doesn't work in the layout editor. Skipping...
        if (isInEditMode()) {
            return;
        }

        TypedArray styledAttrs = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String fontName = styledAttrs.getString(R.styleable.CustomTextView_typeface);
        styledAttrs.recycle();
        if (null != fontName) {
            // Check whether font exist in cache
            Typeface typeface = mTypefaceCache.get(fontName);
            // If typeface doesn't exist in cache then create it from assets
            if (null == typeface) {
                typeface = Typeface.createFromAsset(context.getAssets(), fontName);
                mTypefaceCache.put(fontName, typeface);
            }
            setTypeface(typeface);
        }
        this.setPaintFlags(this.getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

}
