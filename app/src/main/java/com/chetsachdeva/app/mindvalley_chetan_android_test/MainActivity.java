package com.chetsachdeva.app.mindvalley_chetan_android_test;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.chetsachdeva.app.mindvalley_chetan_android_test.adapters.PinsAdapter;
import com.chetsachdeva.app.mindvalley_chetan_android_test.base.BaseActivity;
import com.chetsachdeva.app.mindvalley_chetan_android_test.pins.PinsContract;
import com.chetsachdeva.app.mindvalley_chetan_android_test.pins.PinsPresenterImpl;
import com.chetsachdeva.app.mindvalley_chetan_android_test.preview.PreviewActivity;
import com.chetsachdeva.app.mindvalley_chetan_android_test.utils.Constants;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.Pin;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.PinsResponse;
import com.chetsachdeva.app.mindvalley_chetan_android_test.widgets.CustomRecyclerView;
import com.chetsachdeva.app.mindvalley_chetan_android_test.widgets.progress.CustomProgressDialog;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;

public class MainActivity extends BaseActivity implements PinsContract.View {

    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.rv_pins)
    CustomRecyclerView rvPins;
    @Bind(R.id.content_main)
    RelativeLayout contentMain;
    @Bind(R.id.fab)
    FloatingActionButton fab;
    @Bind(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    List<Pin> pinsList;
    PinsAdapter pinsAdapter;
    PinsContract.Presenter presenter;
    CustomProgressDialog mProgressDialog;

    public static final String pins_code = "wgkJgazE";

    @Override
    protected int getLayoutRes() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new PinsPresenterImpl(this);

        setToolbar();
        setSwipeRefreshLayout();
        setRecyclerView();
        getPins();
    }

    public void setSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(
                R.color.colorPrimaryDark,
                R.color.colorPrimary,
                R.color.colorPrimaryLight);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPins();
            }
        });
    }


    public void setToolbar() {
        if (null != toolbar) {
            setSupportActionBar(toolbar);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayShowTitleEnabled(false);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
            toolbar.setTitle(Constants.MIND_VALLEY);
        }
    }

    public void getPins() {
        presenter.getPins(pins_code);
    }

    public void setRecyclerView() {
        final StaggeredGridLayoutManager staggeredGridLayoutManager = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        staggeredGridLayoutManager.setGapStrategy(StaggeredGridLayoutManager.GAP_HANDLING_NONE);
        staggeredGridLayoutManager.invalidateSpanAssignments();
        rvPins.setLayoutManager(staggeredGridLayoutManager);
        rvPins.setHasFixedSize(true);

        if (null == pinsList) {
            pinsList = new ArrayList<>();
        }
        if (null == pinsAdapter) {
            pinsAdapter = new PinsAdapter(pinsList, this, this);
        }
        rvPins.setAdapter(pinsAdapter);
    }

    @Override
    public void onGetPinsSuccess(PinsResponse pinsResponse) {
        if (null != pinsResponse && null != pinsResponse.getPins() && pinsResponse.getPins().size() > 0) {
            pinsList.addAll(pinsResponse.getPins());
            pinsAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onGetPinsError(String message) {
        showToast(message);
    }

    @Override
    public void onPinClicked(int position) {
        if (null != pinsList.get(position) && null != pinsList.get(position).getUrls()
                && !TextUtils.isEmpty(pinsList.get(position).getUrls().getFull())) {
            Intent intent = new Intent(this, PreviewActivity.class);
            intent.putExtra(Constants.IMAGE_URL, pinsList.get(position).getUrls().getRegular());
            startActivity(intent);
        }
    }

    @Override
    public void showProgress() {
        if (mProgressDialog == null) {
            mProgressDialog = new CustomProgressDialog(this);
        }
        if (!mProgressDialog.isShowing()) {
            mProgressDialog.show();
        }
    }

    @Override
    public void hideProgress() {
        if (null != mProgressDialog) {
            if (mProgressDialog.isShowing()) {
                mProgressDialog.dismiss();
            }
        }
        if (null == swipeRefreshLayout) {
            swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefreshLayout);
        }
        if (null != swipeRefreshLayout && swipeRefreshLayout.isRefreshing()) {
            pinsList.clear();
            swipeRefreshLayout.setRefreshing(false);
        }
    }

    @Override
    public void showToast(String message) {
        if (!TextUtils.isEmpty(message)) {
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }
}
