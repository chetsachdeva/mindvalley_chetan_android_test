package com.chetsachdeva.app.mindvalley_chetan_android_test.utils;

/**
 * Created by chetsachdeva on 13/11/16.
 */

public class Constants {

    public static final String IMAGE_URL="image_url";
    public static final String IMAGE_COLOR="image_color";
    public static final String INTERNET_NOT_FOUND="Internet not found.";
    public static final String SOMETHING_WENT_WRONG="Something went wrong.";
    public static final String MIND_VALLEY="MindValley";
}
