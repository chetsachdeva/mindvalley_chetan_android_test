package com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models;

import com.google.gson.annotations.Expose;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chetsachdeva on 13/11/16.
 */

public class PinsResponse {

    @Expose
    List<Pin> pins = new ArrayList<>();

    public PinsResponse() {
    }

    public List<Pin> getPins() {
        return pins;
    }

    public void setPins(List<Pin> pins) {
        this.pins = pins;
    }
}
