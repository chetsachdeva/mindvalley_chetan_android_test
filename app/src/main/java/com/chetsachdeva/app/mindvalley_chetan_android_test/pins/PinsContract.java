package com.chetsachdeva.app.mindvalley_chetan_android_test.pins;

import com.chetsachdeva.app.mindvalley_chetan_android_test.base.BaseView;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.Pin;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.PinsResponse;

/**
 * Created by chetsachdeva on 13/11/16.
 */

public interface PinsContract {

    void getPins(String pinsCode, Listener listener);

    interface View extends BaseView {
        void onGetPinsSuccess(PinsResponse pinsResponse);

        void onGetPinsError(String message);

        void onPinClicked(int position);
    }

    interface Presenter {
        void getPins(String pinsCode);
    }

    interface Listener {
        void onGetPinsSuccess(PinsResponse pinsResponse);

        void onGetPinsError(String message);
    }
}
