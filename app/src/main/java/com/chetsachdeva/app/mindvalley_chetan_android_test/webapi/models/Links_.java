
package com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models;

public class Links_ {

    public String self;
    public String photos;

    public String getSelf() {
        return self;
    }

    public void setSelf(String self) {
        this.self = self;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }
}
