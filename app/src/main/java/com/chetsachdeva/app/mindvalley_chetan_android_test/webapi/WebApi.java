package com.chetsachdeva.app.mindvalley_chetan_android_test.webapi;

import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.Pin;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.PinsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface WebApi {


    String API = " http://pastebin.com/";
    String apiVersion = "";

    @GET(apiVersion + "raw/{pins_code}")
    Call<List<Pin>> getPins(@Path("pins_code") String pinsCode);

}
