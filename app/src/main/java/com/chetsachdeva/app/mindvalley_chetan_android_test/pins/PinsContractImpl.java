package com.chetsachdeva.app.mindvalley_chetan_android_test.pins;

import com.chetsachdeva.app.mindvalley_chetan_android_test.utils.Constants;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.RetrofitClient;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.Pin;
import com.chetsachdeva.app.mindvalley_chetan_android_test.webapi.models.PinsResponse;

import java.io.IOException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by chetsachdeva on 13/11/16.
 */

public class PinsContractImpl implements PinsContract {
    @Override
    public void getPins(final String pinsCode, final Listener listener) {

        RetrofitClient.get().getPins(pinsCode).enqueue(new Callback<List<Pin>>() {
            @Override
            public void onResponse(Call<List<Pin>> call, Response<List<Pin>> response) {
                if (response.isSuccessful()) {
                    PinsResponse pinsResponse = new PinsResponse();
                    pinsResponse.setPins(response.body());
                    listener.onGetPinsSuccess(pinsResponse);
                } else {
                    listener.onGetPinsError(response.errorBody().toString());
                }
            }

            @Override
            public void onFailure(Call<List<Pin>> call, Throwable t) {
                if (t instanceof IOException) {
                    listener.onGetPinsError(Constants.INTERNET_NOT_FOUND);
                } else {
                    listener.onGetPinsError(t.getLocalizedMessage());
                }
            }
        });
    }
}
